const demoData = {
  rooms: {
    'test':{
      id: 'test',
      users:{
        'oRfrsl837kXhAvgGOxOFYsRf3111': {
          id: 'oRfrsl837kXhAvgGOxOFYsRf111',
          name: 'Filip',
          files: {
            '11111111111':{
                id: '11111111111',
                name: 'firstfile',
                type: 'rar',
             },
             '22222222222':{
                 id: '22222222222',
                 name: 'secondFile',
                 type: 'pdf',
              }
          }
        },
        'oRfrsl837kXhAvgGOxOFYsRf3222': {
          id: 'oRfrsl837kXhAvgGOxOFYsRf3222',
          name: 'Peter',
          files: {
            '11111111111':{
                id: '11111111111',
                name: 'firstfile',
                type: 'rar',
             },
             '22222222222':{
                 id: '22222222222',
                 name: 'secondFile',
                 type: 'pdf',
              }
          }
        }
      }
    }
  }
}

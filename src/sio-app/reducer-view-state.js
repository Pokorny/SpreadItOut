(() => {


  function relationId(state, action) {
    if (!state) {
      state = null;
    }
    return state;
  }

  function currentUser(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
        case 'SET_USER':
          return action.user;
        default:
          return state;
    }
  }

  function addFile(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
        case 'ADD_FILE':
          return action.file;
        default:
          return state;
    }
  }
  function removeFile(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
        case 'DELETE_FILE':
          return action.dbId;
        default:
          return state;
    }
  }



  function avaibleFiles(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
      case 'PREPARE_TO_DOWNLOAD':
        return state;
        break;
      default:
        return state;
    }

  }

  function temp(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
      case 'ROOM_NOT_EXIST':
        console.log("ROOM_NOT_EXIST");
        return state;
      case 'PRINT_ERROR':
        console.log(action.errMsg);
        return state;
      case 'REFRECH_MEMBER':
        //console.log("refreshing member");
        SioParse.refreshMembers();
        return state;
      case 'REFRECH_FILES':
        SioParse.refreshFiles();
        return state;


      default:
        return state;
    }
  }

  function isLoadingActive(state, action) {
    if (!state) {
      state = false;
    }
    switch (action.type) {
      case 'LOADING_STATUS':
        return action.isActive;
      default:
        return state;
    }
  }

  function downloadingTorrent(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
      case 'START_DOWNLOAD':
        return action.torrent;
      default:
        return state;
    }
  }


  window.reducers = window.reducers || {};
  reducers.viewsState = Redux.combineReducers({
    relationId,
    currentUser,
    addFile,
    removeFile,
    downloadingTorrent,


    avaibleFiles,
    temp,
    isLoadingActive,


  });
})();

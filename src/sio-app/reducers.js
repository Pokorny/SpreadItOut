
function screen(state, action) {
  if (!state) {
    state = 'welcome';
  }

  switch (action.type) {
    case 'CHANGE_SCREEN':
      return action.screen || 'login';




    case 'SHOW_SETTINGS':
      return 'settings';
    case 'SHOW_USERS':
      return 'users';
    case 'SHOW_ADD_TRANSCRIPT':
      return 'add-transcript';
    case 'SHOW_WORKER_HISTORY':
      return 'worker-history';
    case 'SHOW_STUDENT_HISTORY':
      return 'student-history';
    case 'SHOW_TRANSCRIPTION_DETAIL':
      return 'transcription-detail';
    default:
      return state;
  }
}

const appReducer = Redux.combineReducers({
  persistent: reducers.persistent,
  viewsState: reducers.viewsState,
  screen
});

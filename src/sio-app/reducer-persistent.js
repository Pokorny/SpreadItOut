(() => {

  function loggedUser(state, action) {
    if (!state) {
      state = null;
    }
    return state;
  }

  function users(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
      case 'SET_USERS':
        return action.users;
      default:
        return state;
    }
  }

  function currentRoom(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
      case 'SET_ROOM':
        return action.room;
      case 'SET_ROOM_NAME':
        return action.name;
      default:
        return state;
    }
  }

  function files(state, action) {
    if (!state) {
      state = null;
    }
    switch (action.type) {
      case 'SET_FILES':
        return action.files;
      default:
        return state;
    }
  }



  window.reducers = window.reducers || {};
  reducers.persistent = Redux.combineReducers({
    loggedUser,
    currentRoom,
    users,
    files,

  });
})();
